OpenGL Graphics Project

quick start:
If you just want to run the program, run bin/opengl_test.exe. The executable is built for Windows x64, and it needs Visual C++ Redistributable for Visual Studio 2013.

compiling:
Mac OS: Use Xcode, add files in mac/graphics-project. Link GLUT and OpenGL libraries. Change working directory to mac/graphics-project. The files here might be out of date.

Windows: Use Visual Studio, add files in graphics-project. Link GLEW and OpenGL libraries (the hard part, google it). Change working directory to graphics-project if possible. If not, copy glsl and png files to the working directory.

Demo:
https://www.youtube.com/watch?v=LCnnwYeFee0

