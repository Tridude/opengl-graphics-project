//
//  cube.h
//  chapter4
//
//  Created by Contrata, Fred on 10/13/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//


#ifndef chapter4_cube_h
#define chapter4_cube_h

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

enum {
    FRONT_LEFT_TOP = 0,
    FRONT_RIGHT_TOP = 1,
    FRONT_RIGHT_BOT = 2,
    FRONT_LEFT_BOT = 3,
    BACK_LEFT_TOP = 4,
    BACK_RIGHT_TOP = 5,
    BACK_RIGHT_BOT = 6,
    BACK_LEFT_BOT = 7
};

class Fcube {
public:
    Fcube(){}
    GLint col;
    mat4 face_matrix;
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
};

class Rcube {
public:
//    Rcube(mat4 init_mat) {
//        cube_matrix = init_mat;
//    }
    Rcube(){}
    //    Rcube(mat4 init_mat, GLint init_color_index) {
    //        cube_matrix = init_mat;
    //        init_color_index = color_index;
    //    }
    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    mat4 cube_matrix;
    Fcube faces[3];
};


#endif
