

#include "CollisionBox.h"

#include "Angel.h"

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;


bool CollisionBox::collides(float other_x, float other_y, float other_z, float other_width, float other_depth, float other_height) {
    // printf("this right edge %f other right edge %f\nthis left edge %f other left edge %f\n",
    //     (pos_x + width/2), (other_x + other_width/2), pos_x-width/2, other_x-other_width/2);
    // printf("this front edge %f other front edge %f\nthis back edge %f other back edge %f\n",
    //     pos_z+width/2, other_z+other_width/2, pos_z-width/2, other_z-other_width/2);
    if (
        (((other_x + other_width/2) >= (pos_x - width/2) && (other_x + other_width/2) <= (pos_x + width/2)) ||
        ((other_x - other_width/2) <= (pos_x + width/2) && (other_x - other_width/2) >= (pos_x - width/2))) &&
        (((other_z + other_depth/2) >= (pos_z - depth/2) && (other_z + other_depth/2) <= (pos_z + depth/2)) ||
        ((other_z - other_depth/2) <= (pos_z + depth/2) && (other_z - other_depth/2) >= (pos_z - depth/2))) &&
        (((other_y + other_height/2) >= (pos_y - height/2) && (other_y + other_height/2) <= (pos_y + height/2)) ||
        ((other_y - other_height/2) <= (pos_y + height/2) && (other_y - other_height/2) >= (pos_y - height/2)))
        ) {
            return true;
    }
    return false;
}

// bool CollisionBox::collides (CollisionBox other) {
//     return true;
// }