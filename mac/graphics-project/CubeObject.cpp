//
//  CubeObject.cpp
//  graphics-project
//
//  Created by Contrata, Fred on 11/12/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//

#include "CubeObject.h"

#include "Angel.h"

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

mat4 CubeObject::GetModelMatrix() {
    return Translate(pos_x, pos_y, pos_z) * RotateZ(rot_z) * RotateX(rot_x) * RotateY(rot_y)
       * Scale(sca_x, sca_y, sca_z);
}