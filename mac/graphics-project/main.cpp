//
// Perspective view of a color cube using LookAt() and Ortho()
//
// Colors are assigned to each vertex and then the rasterizer interpolates
//   those colors across the triangles.
//

#include "Angel.h"
#include "cube.h"
#include "CollisionBox.h"
#include "CubeObject.h"
#include "SphereObject.h"
#include "ModelObject.h"
#include <unistd.h>
#include <math.h>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;
GLuint loadBMP_custom(unsigned char** data, int* w, int* h, const char * imagepath);

const int NumTimesToSubdivide = 5;
const int NumTriangles        = 4096;  // (4 faces)^(NumTimesToSubdivide + 1)
const int NumVerticesSphere   = 3 * NumTriangles;

const float PI = 3.14159265;


vec3 cube_locations[4] = {
    vec3(0.0, 0.0, 3),
    vec3(3.0, 0.0, 0),
    vec3(0.0, 0.0, -3),
    vec3(-3.0, 0.0, 0)
};

color4 light_ambient = color4(0.9, 0.9, 0.9, 1.0);
color4 light_diffuse = color4(0.9, 0.9, 0.9, 1.0);
color4 light_specular = color4(1.0, 1.0, 1.0, 1.0);
point4 light_position = point4(2.0, 8.0, 2.0, 0.0);
float material_shininess = 20.0;
float color_alpha = 1.0;

color4 sphere_ambient;
color4 sphere_diffuse;
color4 sphere_specular;

//
GLuint program;
// model-view matrix uniform shader variable location
GLuint model;
GLuint view;
// projection matrix uniform shader variable location
GLuint projection;
// cube environment
GLuint texture_2d;
GLuint texture_cube;
GLuint texture_type;
// lighting
GLuint light_loc;
GLuint light_source_ambient;
GLuint light_source_diffuse;
GLuint light_source_specular;
GLuint shininess;
GLuint ambient_p;
GLuint diffuse_p;
GLuint specular_p;
GLuint alpha_loc;

// Projection transformation parameters
GLfloat  zNear = 0.1, zFar = 150.0;

// window
int window_width = 1280;
int window_height = 720;
// fov
float fov = 60.0;
float aspect_ratio = (float)window_width / window_height;

// camera hitbox
float camera_xrot = 0.0;
float camera_yrot = 0.0;
float camera_zrot = 0.0;
float camera_xpos = 0.0;
float camera_ypos = 0.0;
float camera_zpos = 0.0;
float camera_width = 1.0;
float camera_depth = 1.0;
float camera_height = 1.0;

// jumping
bool not_jumping = true;
bool hit_max_jump = false;
float max_jump_distance = 4.0;
float jump_distance = 0.0;
float camera_ypos_velocity = 0.0;

// mouse movement
bool mouse_camera_move = false;
float xPrev = -1.0;
float yPrev = -1.0;
bool mouse_warped = false;

// keyboard
bool keys_down[256];

// change time
int prev_time_since_start = 0;
int time_since_start = 0;
int delta_time = 0;

// hit boxes
CollisionBox collision_boxes[4];
int number_of_collision_boxes = 4;

//
const int number_of_model_objects = 4;
ModelObject model_objects[number_of_model_objects];


//
const int number_of_textures = 13;
GLuint textures[number_of_textures];
const int animation_textures = 3;
int animation_total_frames[animation_textures];
float animation_times[animation_textures];
int animation_current_frames[animation_textures];
float animation_counters[animation_textures];
int animation_texture_start_index[animation_textures];

//
Rcube cubes[8];
//----------------------------------------------------------------------------

//sphere
point4 points_sphere[NumVerticesSphere];
vec3   normals_sphere[NumVerticesSphere];
vec2   tex_coords_sphere[NumVerticesSphere];

int Index_sphere = 0;
void triangle( const point4& a, const point4& b, const point4& c )
{
    normals_sphere[Index_sphere] = vec3(a.x,a.y,a.z);
    points_sphere[Index_sphere] = a;    
    tex_coords_sphere[Index_sphere] = vec2( (acos(a.x / sqrt(a.x*a.x + a.z*a.z)) + PI)/(2*PI), 0.5 - asinf(a.y)/PI );
    // tex_coords_sphere[Index_sphere] = vec2( (acos(a.x / sqrt(a.x*a.x + a.z*a.z)) + PI)/(2*PI), acosf(a.y)/PI);
    // tex_coords_sphere[Index_sphere] = vec2( 0.5 + atan2f(a.z, a.x)/(2*PI), acosf(a.y)/PI);
    // tex_coords_sphere[Index_sphere] = vec2( (0.5*PI + atan2f(-a.z, a.x))/(PI), acosf(a.y)/PI);
    Index_sphere++;
    
    normals_sphere[Index_sphere] = vec3(b.x,b.y,b.z);
    points_sphere[Index_sphere] = b;
    tex_coords_sphere[Index_sphere] = vec2( (acos(b.x / sqrt(b.x*b.x + b.z*b.z)) + PI)/(2*PI), 0.5 - asinf(b.y)/PI );
    // tex_coords_sphere[Index_sphere] = vec2( (acos(b.x / sqrt(b.x*b.x + b.z*b.z)) + PI)/(2*PI), acosf(b.y)/PI);
    // tex_coords_sphere[Index_sphere] = vec2( 0.5 + atan2f(b.z, b.x)/(2*PI), acosf(b.y)/PI);
    // tex_coords_sphere[Index_sphere] = vec2( (0.5*PI + atan2f(-b.z, b.x))/(PI), acosf(b.y)/PI);
    Index_sphere++;
    
    normals_sphere[Index_sphere] = vec3(c.x,c.y,c.z);
    points_sphere[Index_sphere] = c;
    tex_coords_sphere[Index_sphere] = vec2( (acos(c.x / sqrt(c.x*c.x + c.z*c.z)) + PI)/(2*PI), 0.5 - asinf(c.y)/PI );
    // tex_coords_sphere[Index_sphere] = vec2( (acos(c.x / sqrt(c.x*c.x + c.z*c.z)) + PI)/(2*PI), acosf(c.y)/PI);    
    // tex_coords_sphere[Index_sphere] = vec2( 0.5 + atan2f(c.z, c.x)/(2*PI), acosf(c.y)/PI);
    // tex_coords_sphere[Index_sphere] = vec2( (0.5*PI + atan2f(-c.z, c.x))/(PI), acosf(c.y)/PI);
    Index_sphere++;
}

//----------------------------------------------------------------------------

point4 unit( const point4& p )
{
    float len = p.x*p.x + p.y*p.y + p.z*p.z;
    
    point4 t;
    if ( len > DivideByZeroTolerance ) {
        t = p / sqrt(len);
        t.w = 1.0;
    }

    return t;
}

void divide_triangle( const point4& a, const point4& b, const point4& c, int count )
{
    if ( count > 0 ) {
        point4 v1 = unit( a + b );
        point4 v2 = unit( a + c );
        point4 v3 = unit( b + c );
        divide_triangle(  a, v1, v2, count - 1 );
        divide_triangle(  c, v2, v3, count - 1 );
        divide_triangle(  b, v3, v1, count - 1 );
        divide_triangle( v1, v3, v2, count - 1 );
    }
    else {
        triangle( a, b, c );
    }
}

void tetrahedron( int count )
{
    point4 v[4] = {
    vec4( 0.0, 0.0, 1.0, 1.0 ),
    vec4( 0.0, 0.942809, -0.333333, 1.0 ),
    vec4( -0.816497, -0.471405, -0.333333, 1.0 ),
    vec4( 0.816497, -0.471405, -0.333333, 1.0 )
    };

    divide_triangle( v[0], v[1], v[2], count );
    divide_triangle( v[3], v[2], v[1], count );
    divide_triangle( v[0], v[3], v[1], count );
    divide_triangle( v[0], v[2], v[3], count );
}



// cube
const int NumVertices = 36; //(6 faces)(2 triangles/face)(3 vertices/triangle)
point4 points[NumVertices];
vec3   normals[NumVertices];
vec2   tex_coords[NumVertices];

// Vertices of a unit cube centered at origin, sides aligned with axes
point4 vertices[8] = {
    point4( -0.5, -0.5,  0.5, 1.0 ),
    point4( -0.5,  0.5,  0.5, 1.0 ),
    point4(  0.5,  0.5,  0.5, 1.0 ),
    point4(  0.5, -0.5,  0.5, 1.0 ),
    point4( -0.5, -0.5, -0.5, 1.0 ),
    point4( -0.5,  0.5, -0.5, 1.0 ),
    point4(  0.5,  0.5, -0.5, 1.0 ),
    point4(  0.5, -0.5, -0.5, 1.0 )
};

// quad generates two triangles for each face and assigns colors
//    to the vertices
int Index = 0;
void quad( int a, int b, int c, int d )
{
    // Initialize temporary vectors along the quad's edge to
    //   compute its face normal 
    vec4 u = vertices[b] - vertices[a];
    vec4 v = vertices[c] - vertices[b];

    vec3 normal = normalize( cross(u, v) );

    normals[Index] = normal; points[Index] = vertices[a]; tex_coords[Index] = vec2( 0.0, 1.0 ); Index++;
    normals[Index] = normal; points[Index] = vertices[b]; tex_coords[Index] = vec2( 0.0, 0.0 ); Index++;
    normals[Index] = normal; points[Index] = vertices[c]; tex_coords[Index] = vec2( 1.0, 0.0 ); Index++;
    normals[Index] = normal; points[Index] = vertices[a]; tex_coords[Index] = vec2( 0.0, 1.0 ); Index++;
    normals[Index] = normal; points[Index] = vertices[c]; tex_coords[Index] = vec2( 1.0, 0.0 ); Index++;
    normals[Index] = normal; points[Index] = vertices[d]; tex_coords[Index] = vec2( 1.0, 1.0 ); Index++;
}


// generate 12 triangles: 36 vertices and 36 colors
void colorcube()
{
    quad( 1, 0, 3, 2 );
    quad( 2, 3, 7, 6 );
    quad( 3, 0, 4, 7 );
    quad( 6, 5, 1, 2 );
    quad( 4, 5, 6, 7 );
    quad( 5, 4, 0, 1 );
}



void initialize_rcube() {

    color4 black = color4( 0.3, 0.3, 0.3, 1.0 );  // black
    color4 white = color4( 0.9, 0.9, 0.9, 1.0 );  // white
    color4 red = color4( 1.0, 0.2, 0.2, 1.0 );  // red
    color4 yellow = color4( 1.0, 1.0, 0.2, 1.0 );  // yellow
    color4 green = color4( 0.2, 1.0, 0.2, 1.0 );  // green
    color4 blue = color4( 0.2, 0.2, 1.0, 1.0 );  // blue
    color4 magenta = color4( 1.0, 0.2, 1.0, 1.0 );  // magenta
    color4 cyan = color4( 0.2, 1.0, 1.0, 1.0 );  // cyan

    color4 ambient_default = color4(0.6, 0.6, 0.6, 1.0);
    color4 diffuse_default = color4(0.9, 0.9, 0.9, 1.0);
    color4 specular_default = color4(1.0, 1.0, 1.0, 1.0);

    sphere_ambient = ambient_default * color4(0.7, 1.0, 0.7, 1.0);
    sphere_diffuse = diffuse_default * color4(0.7, 1.0, 0.7, 1.0);
    sphere_specular = specular_default * color4(0.7, 1.0, 0.7, 1.0);

    for (int i=0; i<8; i++) {
        cubes[i].material_ambient =  ambient_default * black;
        cubes[i].material_diffuse = diffuse_default * black;
        cubes[i].material_specular =  specular_default * black;
    }
    cubes[0].cube_matrix = Translate(-0.50, 0.50, 0.50);
    // cubes[0].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[0].material_diffuse = color4(0.3, 0.3, 0.3, 1.0);
    // cubes[0].material_specular = color4(0.4, 0.4, 0.4, 1.0);
    cubes[0].faces[0].face_matrix = Translate(-0.50, 0.50, 1.00) * Scale(0.8, 0.8, 0.01);
    cubes[0].faces[0].material_ambient = ambient_default * red;
    cubes[0].faces[0].material_diffuse = diffuse_default * red;
    cubes[0].faces[0].material_specular = specular_default * red;
    cubes[0].faces[1].face_matrix = Translate(-0.50, 1.00, 0.50) * Scale(0.8, 0.01, 0.8);
    cubes[0].faces[1].material_ambient = ambient_default * yellow;
    cubes[0].faces[1].material_diffuse = diffuse_default * yellow;
    cubes[0].faces[1].material_specular = specular_default * yellow;
    cubes[0].faces[2].face_matrix = Translate(-1.00, 0.50, 0.50) * Scale(0.01, 0.8, 0.8);
    cubes[0].faces[2].material_ambient = ambient_default * green;
    cubes[0].faces[2].material_diffuse = diffuse_default * green;
    cubes[0].faces[2].material_specular = specular_default * green;
    
    cubes[1].cube_matrix = Translate(0.50, 0.50, 0.50);
    // cubes[1].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[1].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[1].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[1].faces[0].face_matrix = Translate(0.50, 0.50, 1.00) * Scale(0.8, 0.8, 0.01);
    cubes[1].faces[0].material_ambient = ambient_default * red;
    cubes[1].faces[0].material_diffuse = diffuse_default * red;
    cubes[1].faces[0].material_specular = specular_default * red;
    cubes[1].faces[1].face_matrix = Translate(0.50, 1.00, 0.50) * Scale(0.8, 0.01, 0.8);
    cubes[1].faces[1].material_ambient = ambient_default * yellow;
    cubes[1].faces[1].material_diffuse = diffuse_default * yellow;
    cubes[1].faces[1].material_specular = specular_default * yellow;
    cubes[1].faces[2].face_matrix = Translate(1.00, 0.50, 0.50) * Scale(0.01, 0.8, 0.8);
    cubes[1].faces[2].material_ambient = ambient_default * cyan;
    cubes[1].faces[2].material_diffuse = diffuse_default * cyan;
    cubes[1].faces[2].material_specular = specular_default * cyan;

    cubes[2].cube_matrix = Translate(0.50, -0.50, 0.50);
    // cubes[2].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[2].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[2].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[2].faces[0].face_matrix = Translate(0.50, -0.50, 1.00) * Scale(0.8, 0.8, 0.01);
    cubes[2].faces[0].material_ambient = ambient_default * red;
    cubes[2].faces[0].material_diffuse = diffuse_default * red;
    cubes[2].faces[0].material_specular = specular_default * red;
    cubes[2].faces[1].face_matrix = Translate(0.50, -1.00, 0.50) * Scale(0.8, 0.01, 0.8);
    cubes[2].faces[1].material_ambient = ambient_default * magenta;
    cubes[2].faces[1].material_diffuse = diffuse_default * magenta;
    cubes[2].faces[1].material_specular = specular_default * magenta;
    cubes[2].faces[2].face_matrix = Translate(1.00, -0.50, 0.50) * Scale(0.01, 0.8, 0.8);
    cubes[2].faces[2].material_ambient = ambient_default * cyan;
    cubes[2].faces[2].material_diffuse = diffuse_default * cyan;
    cubes[2].faces[2].material_specular = specular_default * cyan;
    
    cubes[3].cube_matrix = Translate(-0.50, -0.50, 0.50);
    // cubes[3].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[3].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[3].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[3].faces[0].face_matrix = Translate(-0.50, -0.50, 1.00) * Scale(0.8, 0.8, 0.01);
    cubes[3].faces[0].material_ambient = ambient_default * red;
    cubes[3].faces[0].material_diffuse = diffuse_default * red;
    cubes[3].faces[0].material_specular = specular_default * red;
    cubes[3].faces[1].face_matrix = Translate(-0.50, -1.00, 0.50) * Scale(0.8, 0.01, 0.8);
    cubes[3].faces[1].material_ambient = ambient_default * magenta;
    cubes[3].faces[1].material_diffuse = diffuse_default * magenta;
    cubes[3].faces[1].material_specular = specular_default * magenta;
    cubes[3].faces[2].face_matrix = Translate(-1.00, -0.50, 0.50) * Scale(0.01, 0.8, 0.8);
    cubes[3].faces[2].material_ambient = ambient_default * green;
    cubes[3].faces[2].material_diffuse = diffuse_default * green;
    cubes[3].faces[2].material_specular = specular_default * green;
    
    cubes[4].cube_matrix = Translate(-0.50, 0.50, -0.50);
    // cubes[4].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[4].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[4].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[4].faces[0].face_matrix = Translate(-0.50, 0.50, -1.00) * Scale(0.8, 0.8, 0.01);
    cubes[4].faces[0].material_ambient = ambient_default * blue;
    cubes[4].faces[0].material_diffuse = diffuse_default * blue;
    cubes[4].faces[0].material_specular = specular_default * blue;
    cubes[4].faces[1].face_matrix = Translate(-0.50, 1.00, -0.50) * Scale(0.8, 0.01, 0.8);
    cubes[4].faces[1].material_ambient = ambient_default * yellow;
    cubes[4].faces[1].material_diffuse = diffuse_default * yellow;
    cubes[4].faces[1].material_specular = specular_default * yellow;
    cubes[4].faces[2].face_matrix = Translate(-1.00, 0.50, -0.50) * Scale(0.01, 0.8, 0.8);
    cubes[4].faces[2].material_ambient = ambient_default * green;
    cubes[4].faces[2].material_diffuse = diffuse_default * green;
    cubes[4].faces[2].material_specular = specular_default * green;
    
    cubes[5].cube_matrix = Translate(0.50, 0.50, -0.50);
    // cubes[5].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[5].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[5].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[5].faces[0].face_matrix = Translate(0.50, 0.50, -1.00) * Scale(0.8, 0.8, 0.01);
    cubes[5].faces[0].material_ambient = ambient_default * blue;
    cubes[5].faces[0].material_diffuse = diffuse_default * blue;
    cubes[5].faces[0].material_specular = specular_default * blue;
    cubes[5].faces[1].face_matrix = Translate(0.50, 1.00, -0.50) * Scale(0.8, 0.01, 0.8);
    cubes[5].faces[1].material_ambient = ambient_default * yellow;
    cubes[5].faces[1].material_diffuse = diffuse_default * yellow;
    cubes[5].faces[1].material_specular = specular_default * yellow;
    cubes[5].faces[2].face_matrix = Translate(1.00, 0.50, -0.50) * Scale(0.01, 0.8, 0.8);
    cubes[5].faces[2].material_ambient = ambient_default * cyan;
    cubes[5].faces[2].material_diffuse = diffuse_default * cyan;
    cubes[5].faces[2].material_specular = specular_default * cyan;

    cubes[6].cube_matrix = Translate(0.50, -0.50, -0.50);
    // cubes[6].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[6].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[6].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[6].faces[0].face_matrix = Translate(0.50, -0.50, -1.00) * Scale(0.8, 0.8, 0.01);
    cubes[6].faces[0].material_ambient = ambient_default * blue;
    cubes[6].faces[0].material_diffuse = diffuse_default * blue;
    cubes[6].faces[0].material_specular = specular_default * blue;
    cubes[6].faces[1].face_matrix = Translate(0.50, -1.00, -0.50) * Scale(0.8, 0.01, 0.8);
    cubes[6].faces[1].material_ambient = ambient_default * magenta;
    cubes[6].faces[1].material_diffuse = diffuse_default * magenta;
    cubes[6].faces[1].material_specular = specular_default * magenta;
    cubes[6].faces[2].face_matrix = Translate(1.00, -0.50, -0.50) * Scale(0.01, 0.8, 0.8);
    cubes[6].faces[2].material_ambient = ambient_default * cyan;
    cubes[6].faces[2].material_diffuse = diffuse_default * cyan;
    cubes[6].faces[2].material_specular = specular_default * cyan;
    
    cubes[7].cube_matrix = Translate(-0.50, -0.50, -0.50);
    // cubes[7].material_ambient = color4(0.2, 0.2, 0.2, 1.0);
    // cubes[7].material_diffuse = color4(0.16, 0.16, 0.16, 1.0);
    // cubes[7].material_specular = color4(0.16, 0.16, 0.16, 1.0);
    cubes[7].faces[0].face_matrix = Translate(-0.50, -0.50, -1.00) * Scale(0.8, 0.8, 0.01);
    cubes[7].faces[0].material_ambient = ambient_default * blue;
    cubes[7].faces[0].material_diffuse = diffuse_default * blue;
    cubes[7].faces[0].material_specular = specular_default * blue;
    cubes[7].faces[1].face_matrix = Translate(-0.50, -1.00, -0.50) * Scale(0.8, 0.01, 0.8);
    cubes[7].faces[1].material_ambient = ambient_default * magenta;
    cubes[7].faces[1].material_diffuse = diffuse_default * magenta;
    cubes[7].faces[1].material_specular = specular_default * magenta;
    cubes[7].faces[2].face_matrix = Translate(-1.00, -0.50, -0.50) * Scale(0.01, 0.8, 0.8);
    cubes[7].faces[2].material_ambient = ambient_default * green;
    cubes[7].faces[2].material_diffuse = diffuse_default * green;
    cubes[7].faces[2].material_specular = specular_default * green;
}

void initialize_collision_boxes() {
    collision_boxes[0].pos_x = 0.0;
    collision_boxes[0].pos_y = 0.0;
    collision_boxes[0].pos_z = 3.0;
    collision_boxes[0].width = 2.0;
    collision_boxes[0].height = 2.0;
    collision_boxes[0].depth = 2.0;

    collision_boxes[1].pos_x = 3.0;
    collision_boxes[1].pos_y = 0.0;
    collision_boxes[1].pos_z = 0.0;
    collision_boxes[1].width = 2.0;
    collision_boxes[1].height = 2.0;
    collision_boxes[1].depth = 2.0;

    collision_boxes[2].pos_x = 0.0;
    collision_boxes[2].pos_y = 0.0;
    collision_boxes[2].pos_z = -3.0;
    collision_boxes[2].width = 2.0;
    collision_boxes[2].height = 2.0;
    collision_boxes[2].depth = 2.0;

    collision_boxes[3].pos_x = -3.0;
    collision_boxes[3].pos_y = 0.0;
    collision_boxes[3].pos_z = 0.0;
    collision_boxes[3].width = 2.0;
    collision_boxes[3].height = 2.0;
    collision_boxes[3].depth = 2.0;
}

void initialize_model_objects() {
    color4 black = color4( 0.3, 0.3, 0.3, 1.0 );  // black
    color4 white = color4( 0.9, 0.9, 0.9, 1.0 );  // white
    color4 red = color4( 1.0, 0.2, 0.2, 1.0 );  // red
    color4 yellow = color4( 1.0, 1.0, 0.2, 1.0 );  // yellow
    color4 green = color4( 0.2, 1.0, 0.2, 1.0 );  // green
    color4 blue = color4( 0.2, 0.2, 1.0, 1.0 );  // blue
    color4 magenta = color4( 1.0, 0.2, 1.0, 1.0 );  // magenta
    color4 cyan = color4( 0.2, 1.0, 1.0, 1.0 );  // cyan

    int i = 0;
    // ground
    model_objects[i].cube_count = 0;
    model_objects[i].cube_objects[0].pos_x = 0.0;
    model_objects[i].cube_objects[0].pos_y = -1.0;
    model_objects[i].cube_objects[0].pos_z = 0.0;
    model_objects[i].cube_objects[0].rot_x = 0.0;
    model_objects[i].cube_objects[0].rot_y = 0.0;
    model_objects[i].cube_objects[0].rot_z = 0.0;
    model_objects[i].cube_objects[0].sca_x = 100.0;
    model_objects[i].cube_objects[0].sca_y = 0.01;
    model_objects[i].cube_objects[0].sca_z = 100.0;
    model_objects[i].cube_objects[0].material_ambient = vec4(0.2, 0.2, 0.2, 1.0);
    model_objects[i].cube_objects[0].material_diffuse = vec4(0.9, 0.9, 0.9, 1.0);
    model_objects[i].cube_objects[0].material_specular = vec4(0.1, 0.1, 0.1, 1.0);
    model_objects[i].cube_objects[0].material_shininess = 10.0;
    model_objects[i].cube_objects[0].texture_index = 11;
    model_objects[i].cube_objects[0].animation_index = 0;
    // sun
    model_objects[i].sphere_count = 1;
    model_objects[i].sphere_objects[0].pos_x = 0.0;
    model_objects[i].sphere_objects[0].pos_y = 45.0;
    model_objects[i].sphere_objects[0].pos_z = 45.0;
    model_objects[i].sphere_objects[0].rot_x = 180.0;
    model_objects[i].sphere_objects[0].rot_y = 0.0;
    model_objects[i].sphere_objects[0].rot_z = 0.0;
    model_objects[i].sphere_objects[0].sca_x = 2.0;
    model_objects[i].sphere_objects[0].sca_y = 2.0;
    model_objects[i].sphere_objects[0].sca_z = 2.0;
    model_objects[i].sphere_objects[0].material_ambient = vec4(0.4, 0.2, 0.2, 1.0);
    model_objects[i].sphere_objects[0].material_diffuse = vec4(0.9, 0.7, 0.7, 1.0);
    model_objects[i].sphere_objects[0].material_specular = vec4(0.9, 0.7, 0.7, 1.0);
    model_objects[i].sphere_objects[0].material_shininess = 10.0;
    model_objects[i].sphere_objects[0].texture_index = 10;
    model_objects[i].sphere_objects[0].animation_index = 0;
    // model_objects[2].collision_count = 0;
    // model_objects[2].collision_boxes[0].pos_x = 3.0;
    // model_objects[2].collision_boxes[0].pos_y = 0.0;
    // model_objects[2].collision_boxes[0].pos_z = 3.0;
    // model_objects[2].collision_boxes[0].width = 1.0;
    // model_objects[2].collision_boxes[0].depth = 1.0;
    // model_objects[2].collision_boxes[0].height = 1.0;
    i++;

    // doge cube
    model_objects[i].cube_count = 1;
    model_objects[i].cube_objects[0].pos_x = 0.0;
    model_objects[i].cube_objects[0].pos_y = 2.0;
    model_objects[i].cube_objects[0].pos_z = -3.0;
    model_objects[i].cube_objects[0].rot_x = 0.0;
    model_objects[i].cube_objects[0].rot_y = 0.0;
    model_objects[i].cube_objects[0].rot_z = 0.0;
    model_objects[i].cube_objects[0].sca_x = 2.0;
    model_objects[i].cube_objects[0].sca_y = 2.0;
    model_objects[i].cube_objects[0].sca_z = 2.0;
    model_objects[i].cube_objects[0].alpha = 0.5;
    model_objects[i].cube_objects[0].material_ambient = vec4(0.2, 0.2, 0.2, 1.0);
    model_objects[i].cube_objects[0].material_diffuse = vec4(0.9, 0.9, 0.9, 1.0);
    model_objects[i].cube_objects[0].material_specular = vec4(0.6, 0.6, 0.6, 1.0);
    model_objects[i].cube_objects[0].material_shininess = 150.0;
    model_objects[i].cube_objects[0].texture_index = 0;
    model_objects[i].cube_objects[0].animation_index = 1;
    model_objects[i].collision_count = 1;
    model_objects[i].collision_boxes[0].pos_x = -0.0;
    model_objects[i].collision_boxes[0].pos_y = -2.0;
    model_objects[i].collision_boxes[0].pos_z = 3.0;
    model_objects[i].collision_boxes[0].width = 2.0;
    model_objects[i].collision_boxes[0].depth = 2.0;
    model_objects[i].collision_boxes[0].height = 2.0;
    i++;

    // nyan cube
    model_objects[i].cube_count = 1;
    model_objects[i].cube_objects[0].pos_x = 3.0;
    model_objects[i].cube_objects[0].pos_y = 0.0;
    model_objects[i].cube_objects[0].pos_z = -3.0;
    model_objects[i].cube_objects[0].rot_x = 0.0;
    model_objects[i].cube_objects[0].rot_y = 0.0;
    model_objects[i].cube_objects[0].rot_z = 0.0;
    model_objects[i].cube_objects[0].sca_x = 2.0;
    model_objects[i].cube_objects[0].sca_y = 2.0;
    model_objects[i].cube_objects[0].sca_z = 2.0;
    model_objects[i].cube_objects[0].alpha = 0.5;
    model_objects[i].cube_objects[0].material_ambient = vec4(0.2, 0.2, 0.2, 1.0);
    model_objects[i].cube_objects[0].material_diffuse = vec4(0.9, 0.9, 0.9, 1.0);
    model_objects[i].cube_objects[0].material_specular = vec4(0.6, 0.6, 0.6, 1.0);
    model_objects[i].cube_objects[0].material_shininess = 150.0;
    model_objects[i].cube_objects[0].texture_index = 0;
    model_objects[i].cube_objects[0].animation_index = 2;
    model_objects[i].collision_count = 1;
    model_objects[i].collision_boxes[0].pos_x = -3.0;
    model_objects[i].collision_boxes[0].pos_y = 0.0;
    model_objects[i].collision_boxes[0].pos_z = 3.0;
    model_objects[i].collision_boxes[0].width = 2.0;
    model_objects[i].collision_boxes[0].depth = 2.0;
    model_objects[i].collision_boxes[0].height = 2.0;
    i++;

    

}

void initialize_textures() {
    animation_total_frames[0] = 0;
    animation_times[0] = 0;
    animation_current_frames[0] = 0;
    animation_counters[0] = 0;
    animation_texture_start_index[0] = 0;
    for (int i=1; i<animation_textures; i++) {
        animation_current_frames[i] = animation_texture_start_index[i];
        animation_counters[i] = 0;
    }


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);

    glGenTextures(number_of_textures, textures);
    unsigned char* pic1 = NULL;
    int w,h;
    
    // doge animation wow
    loadBMP_custom(&pic1, &w, &h, "doge.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "doge_1.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "doge_2.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "doge_3.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[3]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    animation_total_frames[1] = 4;
    animation_times[1] = 1000;
    animation_texture_start_index[1] = 0;

    // nyan cat
    loadBMP_custom(&pic1, &w, &h, "nya_1.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[4]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "nya_2.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[5]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "nya_3.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[6]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "nya_4.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[7]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "nya_5.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[8]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    loadBMP_custom(&pic1, &w, &h, "nya_6.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[9]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    animation_total_frames[2] = 6;
    animation_times[2] = 200;
    animation_texture_start_index[2] = 4;

    // sun
    loadBMP_custom(&pic1, &w, &h, "texture_sun.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[10]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // ground grass
    loadBMP_custom(&pic1, &w, &h, "grass.bmp");
    glBindTexture(GL_TEXTURE_2D, textures[11]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_BGR, GL_UNSIGNED_BYTE, pic1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


    unsigned char* negx_s = NULL;
    printf("ksy\n");
    loadBMP_custom(&negx_s, &w, &h, "doge.bmp");
    unsigned char* negy_s = NULL;
    loadBMP_custom(&negy_s, &w, &h, "doge.bmp");
    unsigned char* negz_s = NULL;
    loadBMP_custom(&negz_s, &w, &h, "doge.bmp");
    unsigned char* posx_s = NULL;
    loadBMP_custom(&posx_s, &w, &h, "doge.bmp");
    unsigned char* posy_s = NULL;
    loadBMP_custom(&posy_s, &w, &h, "doge.bmp");
    unsigned char* posz_s = NULL;
    loadBMP_custom(&posz_s, &w, &h, "doge.bmp");
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textures[12]);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, posx_s);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, posy_s);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, posz_s);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, negx_s);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, negy_s);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, negz_s);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
}
//----------------------------------------------------------------------------

// OpenGL initialization
GLuint vPosition;
GLuint vNormal;
GLuint vTexCoord;

void
init()
{

    colorcube();
    tetrahedron(NumTimesToSubdivide);
    
    initialize_rcube();
    initialize_collision_boxes();
    initialize_model_objects();
    initialize_textures();

    program = InitShader("vshader53.glsl", "fshader41.glsl");
    glUseProgram(program);

    // Create a vertex array object
    GLuint vao;
    glGenVertexArraysAPPLE( 1, &vao );
    glBindVertexArrayAPPLE( vao );

    // Create and initialize a buffer object
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(points) + sizeof(normals) + sizeof(tex_coords) + sizeof(points_sphere) + sizeof(normals_sphere) + sizeof(tex_coords_sphere),
        NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(points), points);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(points), sizeof(normals), normals);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals), sizeof(tex_coords), tex_coords);
    glBufferSubData(GL_ARRAY_BUFFER, 
        sizeof(points) + sizeof(normals) + sizeof(tex_coords), 
        sizeof(points_sphere), points_sphere);
    glBufferSubData(GL_ARRAY_BUFFER,
        sizeof(points) + sizeof(normals) + sizeof(tex_coords) + sizeof(points_sphere),
        sizeof(normals_sphere), normals_sphere);
    glBufferSubData(GL_ARRAY_BUFFER,
        sizeof(points) + sizeof(normals) + sizeof(tex_coords) + sizeof(points_sphere) + sizeof(normals_sphere),
        sizeof(tex_coords_sphere), tex_coords_sphere);

    // Load shaders and use the resulting shader program
    
    // set up vertex arrays

    vPosition = glGetAttribLocation( program, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    
    vNormal = glGetAttribLocation( program, "vNormal" );
    glEnableVertexAttribArray( vNormal );
    glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points)) );

    vTexCoord = glGetAttribLocation(program, "vTexCoord");
    glEnableVertexAttribArray(vTexCoord);
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(points) + sizeof(normals)));

    glUniform1i( glGetUniformLocation(program, "texture"), 0 );

    model = glGetUniformLocation(program, "model");
    view = glGetUniformLocation(program, "view");
    projection = glGetUniformLocation(program, "projection");

    texture_2d = glGetUniformLocation(program, "texture_2d");
    texture_cube = glGetUniformLocation(program, "texture_cube");
    texture_type = glGetUniformLocation(program, "texture_type");
    
    shininess = glGetUniformLocation(program, "Shininess");

    light_source_ambient = glGetUniformLocation(program, "AmbientLight");
    light_source_diffuse = glGetUniformLocation(program, "DiffuseLight");
    light_source_specular = glGetUniformLocation(program, "SpecularLight");

    ambient_p = glGetUniformLocation(program, "AmbientProduct");
    diffuse_p = glGetUniformLocation(program, "DiffuseProduct");
    specular_p = glGetUniformLocation(program, "SpecularProduct");

    light_loc = glGetUniformLocation(program, "LightPosition");

    alpha_loc = glGetUniformLocation(program, "alpha");

    glUniform4fv(light_loc, 1, light_position);
    glUniform1f(shininess, material_shininess);
    
    glEnable( GL_BLEND );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.9, 1.0, 0.9, 1.0); 
}



//----------------------------------------------------------------------------

void display_cube(CubeObject cube_object) {
    int texture_index = 0;
    if (cube_object.animation_index != 0) {
        texture_index = animation_current_frames[cube_object.animation_index];
    } else {
        texture_index = cube_object.texture_index;
    }
    glBindTexture(GL_TEXTURE_2D, textures[texture_index]);
    glUniformMatrix4fv(model, 1, GL_TRUE, cube_object.GetModelMatrix());
    glUniform1f(shininess, cube_object.material_shininess);
    glUniform1f(alpha_loc, cube_object.alpha);
    glUniform4fv(ambient_p, 1, light_ambient * cube_object.material_ambient);
    glUniform4fv(diffuse_p, 1, light_diffuse * cube_object.material_diffuse);
    glUniform4fv(specular_p, 1, light_specular * cube_object.material_specular);
    glDrawArrays(GL_TRIANGLES, 0, NumVertices);
}

void display_sphere(SphereObject sphere_object) {
    int texture_index = 0;
    if (sphere_object.animation_index != 0) {
        texture_index = animation_current_frames[sphere_object.animation_index];
    } else {
        texture_index = sphere_object.texture_index;
    }
    glBindTexture(GL_TEXTURE_2D, textures[texture_index]);
    glUniformMatrix4fv(model, 1, GL_TRUE, sphere_object.GetModelMatrix());
    glUniform1f(shininess, sphere_object.material_shininess);
    glUniform1f(alpha_loc, sphere_object.alpha);
    glUniform4fv(ambient_p, 1, light_ambient * sphere_object.material_ambient);
    glUniform4fv(diffuse_p, 1, light_diffuse * sphere_object.material_diffuse);
    glUniform4fv(specular_p, 1, light_specular * sphere_object.material_specular);
    glDrawArrays(GL_TRIANGLES, 0, NumVerticesSphere);
}

void
display( void )
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    

    
    mat4 mv = RotateX(camera_xrot) * RotateY(camera_yrot) * Translate(camera_xpos, camera_ypos, camera_zpos);

    glUniformMatrix4fv(view, 1, GL_TRUE, mv);

    mat4 p = Perspective(fov, aspect_ratio, zNear, zFar);
    glUniformMatrix4fv(projection, 1, GL_TRUE, p);

    glUniform4fv(light_loc, 1, light_position);
    glUniform4fv(light_source_ambient, 1, light_ambient);
    glUniform4fv(light_source_diffuse, 1, light_diffuse);
    glUniform4fv(light_source_specular, 1, light_specular);
    
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0,
        BUFFER_OFFSET(sizeof(points)));
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0,
        BUFFER_OFFSET(sizeof(points) + sizeof(normals)));
    glUniform1i(texture_cube, 1);
    glUniform1i(texture_type, 1);
    glActiveTexture(GL_TEXTURE1);
    glUniform1f(alpha_loc, 1.0);
    glUniformMatrix4fv(model, 1, GL_TRUE, Translate(0.0, 24.0, 0.0)*Scale(50.0, 50.0, 50.0));
    glBindTexture(GL_TEXTURE_CUBE_MAP, textures[12]);
    glDrawArrays(GL_TRIANGLES, 0, NumVertices);

    glUniform1i(texture_2d, 0);
    glUniform1i(texture_type, 0);
    glActiveTexture(GL_TEXTURE0);

    for (int i=0; i<number_of_model_objects; i++) {
        // set up vertex arrays for cube
        glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
        glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0,
            BUFFER_OFFSET(sizeof(points)));
        glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0,
            BUFFER_OFFSET(sizeof(points) + sizeof(normals)));
        for (int k=0; k<model_objects[i].cube_count; k++) {
            display_cube(model_objects[i].cube_objects[k]);
        }
        
        // set up vertex arrays for sphere
        glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0,
            BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(tex_coords)));
        glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0,
            BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(tex_coords)+sizeof(points_sphere)));
        glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, GL_FALSE, 0,
            BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(tex_coords)+sizeof(points_sphere)+sizeof(normals_sphere)));
        for (int k=0; k<model_objects[i].sphere_count; k++) {
            display_sphere(model_objects[i].sphere_objects[k]);
        }
    }

    



    // for (int k=0; k<4; k++) {
    //     mat4 translate_matrix = Translate(cube_locations[k]);
    //     for (int i=0; i<8; i++) {
    //         color_alpha = 1.0;
    //         glUniform4fv(ambient_p, 1, light_ambient * cubes[i].material_ambient);
    //         glUniform4fv(diffuse_p, 1, light_diffuse * cubes[i].material_diffuse);
    //         glUniform4fv(specular_p, 1, light_specular * cubes[i].material_specular);
    //         glUniformMatrix4fv(model, 1, GL_TRUE, translate_matrix * cubes[i].cube_matrix);
    //         //glUniform1f(alpha_loc, color_alpha);
    //         glDrawArrays(GL_TRIANGLES, 0, NumVertices);
    //         color_alpha = 1.0;
    //         for (int j=0; j<3; j++) {
    //             //usleep(100);
    //             glUniform4fv(ambient_p, 1, light_ambient * cubes[i].faces[j].material_ambient);
    //             glUniform4fv(diffuse_p, 1, light_diffuse * cubes[i].faces[j].material_diffuse);
    //             glUniform4fv(specular_p, 1, light_specular * cubes[i].faces[j].material_specular);
    //             glUniformMatrix4fv(model, 1, GL_TRUE, translate_matrix * cubes[i].faces[j].face_matrix);
    //             //glUniform1f(alpha_loc, color_alpha);
    //             glDrawArrays(GL_TRIANGLES, 0, NumVertices);
    //             //usleep(100);
    //         }
    //     }
    // }


    for (int i=1; i<animation_textures; i++) {
        animation_counters[i] += delta_time;
        while (animation_counters[i] >= animation_times[i]) {
            animation_counters[i] -= animation_times[i];
            animation_current_frames[i] += 1;
            if (animation_current_frames[i] >= animation_texture_start_index[i] + animation_total_frames[i]) {
                animation_current_frames[i] = animation_texture_start_index[i];
            }
        }
    }


    glutSwapBuffers();
}

//----------------------------------------------------------------------------
int indicies[4];
char rotate_axis = 'x';
float rotate_by = 0;
bool rotate_status = false;

void SetRotation() {
    rotate_status = true;
}

void RotateRubick(mat4 rot) {
    for (int i=0; i<4; i++) {
        cubes[indicies[i]].cube_matrix = rot * cubes[indicies[i]].cube_matrix;
        for (int j=0; j<3; j++) {
            cubes[indicies[i]].faces[j].face_matrix = rot * cubes[indicies[i]].faces[j].face_matrix;
        }
    }
}

bool check_camera_collision(float x_change, float y_change, float z_change) {
    // for (int i = 0; i < number_of_collision_boxes; i++) {
    //     if (collision_boxes[i].collides(camera_xpos + x_change, camera_ypos + y_change, camera_zpos + z_change, camera_width, camera_depth, camera_height)) {
    //         return true;
    //     }
    // }
    for (int i=0; i<number_of_model_objects; i++) {
        for (int k=0; k<model_objects[i].collision_count; k++) {
            if (model_objects[i].collision_boxes[k].collides(camera_xpos + x_change, camera_ypos + y_change, camera_zpos + z_change, camera_width, camera_depth, camera_height)) {
                return true;
            }
        }
    }
    return false;
}

void
keyboard( unsigned char key, int x, int y )
{

    switch( key ) {
        case 033: // Escape Key
        case 'q': case 'Q':
            exit( EXIT_SUCCESS );
            break;
        case 'z': zNear  *= 1.1; zFar *= 1.1; break;
        case 'Z': zNear *= 0.9; zFar *= 0.9; break;
        case 'c': if (light_ambient[0] < 1.00) {light_ambient[0] += 0.05;} break;
        case 'C': if (light_ambient[0] > 0.00) {light_ambient[0] -= 0.05;} break;
        case 'v': if (light_ambient[1] < 1.00) {light_ambient[1] += 0.05;} break;
        case 'V': if (light_ambient[1] > 0.00) {light_ambient[1] -= 0.05;} break;
        case 'b': if (light_ambient[2] < 1.00) {light_ambient[2] += 0.05;} break;
        case 'B': if (light_ambient[2] > 0.00) {light_ambient[2] -= 0.05;} break;

        case 'j': light_position[0] += 0.1; break;
        case 'J': light_position[0] -= 0.1; break;
        case 'k': light_position[1] += 0.1; break;
        case 'K': light_position[1] -= 0.1; break;
        case 'l': light_position[2] += 0.1; break;
        case 'L': light_position[2] -= 0.1; break;

        case '+': if (material_shininess < 100.0) {material_shininess += 1.0;} break;
        case '-': if (material_shininess > 000.0) {material_shininess -= 1.0;} break;


    }

    if (rotate_status == false) {
        Rcube cube_front_left_top = cubes[FRONT_LEFT_TOP];
        Rcube cube_front_right_top = cubes[FRONT_RIGHT_TOP];
        Rcube cube_front_right_bot = cubes[FRONT_RIGHT_BOT];
        Rcube cube_front_left_bot = cubes[FRONT_LEFT_BOT];
        Rcube cube_back_left_top = cubes[BACK_LEFT_TOP];
        Rcube cube_back_right_top = cubes[BACK_RIGHT_TOP];
        Rcube cube_back_right_bot = cubes[BACK_RIGHT_BOT];
        Rcube cube_back_left_bot = cubes[BACK_LEFT_BOT];
        switch (key) {
            case '1':
                indicies[0] = FRONT_LEFT_TOP;
                indicies[1] = FRONT_LEFT_BOT;
                indicies[2] = BACK_LEFT_TOP;
                indicies[3] = BACK_LEFT_BOT;
                cubes[FRONT_LEFT_TOP] = cube_back_left_top;
                cubes[FRONT_LEFT_BOT] = cube_front_left_top;
                cubes[BACK_LEFT_TOP] = cube_back_left_bot;
                cubes[BACK_LEFT_BOT] = cube_front_left_bot;
                rotate_axis = 'x';
                rotate_by += 90.0;
                SetRotation();
                break;
            case '2':
                indicies[0] = FRONT_RIGHT_TOP;
                indicies[1] = FRONT_RIGHT_BOT;
                indicies[2] = BACK_RIGHT_TOP;
                indicies[3] = BACK_RIGHT_BOT;
                cubes[FRONT_RIGHT_TOP] = cube_back_right_top;
                cubes[FRONT_RIGHT_BOT] = cube_front_right_top;
                cubes[BACK_RIGHT_TOP] = cube_back_right_bot;
                cubes[BACK_RIGHT_BOT] = cube_front_right_bot;
                rotate_axis = 'x';
                rotate_by += 90.0;
                SetRotation();
                break;
            case '4':
                indicies[0] = FRONT_LEFT_TOP;
                indicies[1] = FRONT_RIGHT_TOP;
                indicies[2] = BACK_LEFT_TOP;
                indicies[3] = BACK_RIGHT_TOP;
                cubes[FRONT_LEFT_TOP] = cube_back_left_top;
                cubes[FRONT_RIGHT_TOP] = cube_front_left_top;
                cubes[BACK_LEFT_TOP] = cube_back_right_top;
                cubes[BACK_RIGHT_TOP] = cube_front_right_top;
                rotate_axis = 'y';
                rotate_by += 90.0;
                SetRotation();
                break;
            case '5':
                indicies[0] = FRONT_LEFT_BOT;
                indicies[1] = FRONT_RIGHT_BOT;
                indicies[2] = BACK_LEFT_BOT;
                indicies[3] = BACK_RIGHT_BOT;
                cubes[FRONT_LEFT_BOT] = cube_back_left_bot;
                cubes[FRONT_RIGHT_BOT] = cube_front_left_bot;
                cubes[BACK_LEFT_BOT] = cube_back_right_bot;
                cubes[BACK_RIGHT_BOT] = cube_front_right_bot;
                rotate_axis = 'y';
                rotate_by += 90.0;
                SetRotation();
                break;
            case '7':
                indicies[0] = FRONT_LEFT_TOP;
                indicies[1] = FRONT_RIGHT_TOP;
                indicies[2] = FRONT_RIGHT_BOT;
                indicies[3] = FRONT_LEFT_BOT;
                cubes[FRONT_LEFT_TOP] = cube_front_right_top;
                cubes[FRONT_RIGHT_TOP] = cube_front_right_bot;
                cubes[FRONT_RIGHT_BOT] = cube_front_left_bot;
                cubes[FRONT_LEFT_BOT] = cube_front_left_top;
                rotate_axis = 'z';
                rotate_by += 90.0;
                SetRotation();
                break;
            case '8':
                indicies[0] = BACK_LEFT_TOP;
                indicies[1] = BACK_RIGHT_TOP;
                indicies[2] = BACK_RIGHT_BOT;
                indicies[3] = BACK_LEFT_BOT;
                cubes[BACK_LEFT_TOP] = cube_back_right_top;
                cubes[BACK_RIGHT_TOP] = cube_back_right_bot;
                cubes[BACK_RIGHT_BOT] = cube_back_left_bot;
                cubes[BACK_LEFT_BOT] = cube_back_left_top;
                rotate_axis = 'z';
                rotate_by += 90.0;
                SetRotation();
                break;
        }
    }
    
    keys_down[key] = true;

}

void
keyboardUp( unsigned char key, int x, int y ) {

    keys_down[key] = false;

}

void keyboardSpecial(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_PAGE_UP:
            fov += 0.2;
            break;
        case GLUT_KEY_PAGE_DOWN:
            fov -= 0.2;
            break;
    }
    printf("%f\n", fov);
}

void mouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        
        if (state == GLUT_DOWN) {
            mouse_camera_move = true;
            xPrev = x;
            yPrev = y;
        } else {
            mouse_camera_move = false;
        }
    }

}

void mouseMove(int x, int y) {
    // if (!mouse_warped) {
    //     float mouse_speed = 0.001 * delta_time;

    //     camera_xrot += (y - window_height/2) * 0.2;
    //     camera_yrot += (x - window_width/2) * 0.2;
    //     if (camera_xrot > 360) { camera_xrot = -360; }
    //     if (camera_xrot < -360) { camera_xrot = 360; }
    //     if (camera_yrot > 360) { camera_yrot = -360; }
    //     if (camera_yrot < -360) { camera_yrot = 360; }

    //     glutWarpPointer(window_width/2, window_height/2);
    //     mouse_warped = true;    
    // } else {
    //     mouse_warped = false;
    // }

    // camera_xrot += (y - yPrev) * 0.2;
    // camera_yrot += (x - xPrev) * 0.2;
    // if (camera_xrot > 360) { camera_xrot = -360; }
    // if (camera_xrot < -360) { camera_xrot = 360; }
    // if (camera_yrot > 360) { camera_yrot = -360; }
    // if (camera_yrot < -360) { camera_yrot = 360; }
    // yPrev = y;
    // xPrev = x;


    if (mouse_camera_move) {
        camera_xrot += (y - yPrev) * 0.2f;
        camera_yrot += (x - xPrev) * 0.2f;
        if (camera_xrot > 70) { camera_xrot = 70; }
        if (camera_xrot < -70) { camera_xrot = -70; }
        if (camera_yrot > 360) { camera_yrot = -360; }
        if (camera_yrot < -360) { camera_yrot = 360; }
        yPrev = y;
        xPrev = x;
    }
}

//----------------------------------------------------------------------------

void camera_movement() {
    float move_speed = 0.005 * delta_time;
    
    if (keys_down['w']) {
        float x_change = move_speed * sin(-camera_yrot * DegreesToRadians);
        float z_change = move_speed * cos(-camera_yrot * DegreesToRadians);
        if (!check_camera_collision(x_change, 0.0, z_change)){
            camera_xpos += x_change;
            camera_zpos += z_change;
        }
    }
    if (keys_down['s']) {
        float x_change = -move_speed * sin(-camera_yrot * DegreesToRadians);
        float z_change = -move_speed * cos(-camera_yrot * DegreesToRadians);
        if (!check_camera_collision(x_change, 0.0, z_change)){
            camera_xpos += x_change;
            camera_zpos += z_change;
        }
    }
    if (keys_down['a']) {
        float x_change = move_speed * sin((90-camera_yrot) * DegreesToRadians);
        float z_change = move_speed * cos((90-camera_yrot) * DegreesToRadians);
        if (!check_camera_collision(x_change, 0.0, z_change)){
            camera_xpos += x_change;
            camera_zpos += z_change;
        }
    }
    if (keys_down['d']) {
        float x_change = -move_speed * sin((90-camera_yrot) * DegreesToRadians);
        float z_change = -move_speed * cos((90-camera_yrot) * DegreesToRadians);
        if (!check_camera_collision(x_change, 0.0, z_change)){
            camera_xpos += x_change;
            camera_zpos += z_change;
        }
    }


    float jump_speed = 0.020 * delta_time;
    float gravity = 0.001 * delta_time;
    
    // printf("camera_ypos: %f\n", camera_ypos);
    // printf("jump: %f gravity: %f\n", jump_speed, gravity);

    if (keys_down[' '] && not_jumping) {
        //printf("jumping\n");
        camera_ypos_velocity -= jump_speed;
        not_jumping = false;
    }
    if (!check_camera_collision(0.0, camera_ypos_velocity + gravity, 0.0) && camera_ypos + camera_ypos_velocity + gravity < 0.0) {
        //printf("not collided\n");
        camera_ypos_velocity += gravity;
    } else {
        //printf("collided\n");
        camera_ypos_velocity = 0.0;
        not_jumping = true;
    }

    camera_ypos += camera_ypos_velocity;

}

void
idle() {
    
    time_since_start = glutGet(GLUT_ELAPSED_TIME);
    delta_time = time_since_start - prev_time_since_start;
    prev_time_since_start = time_since_start;

    camera_movement();

    if (rotate_status == true && rotate_by > 0) {
        if (rotate_axis == 'x') {
            RotateRubick(RotateX(5));
        } else if (rotate_axis == 'y') {
            RotateRubick(RotateY(5));
        } else if (rotate_axis == 'z') {
            RotateRubick(RotateZ(5));
        }
        rotate_by -= 5.0;
        if (rotate_by <= 0) {
            rotate_status = false;
        }
    }
    
    
    glutPostRedisplay();
}

void
reshape( int width, int height )
{
    glViewport( 0, 0, width, height );
}

//----------------------------------------------------------------------------

int
main( int argc, char **argv )
{
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( window_width, window_height );
    glutCreateWindow( "Nice Boat" );

    init();

    glutDisplayFunc( display );
    glutIdleFunc(idle);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardUp);
    glutSpecialFunc(keyboardSpecial);
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMove);
    glutReshapeFunc(reshape);

    glutMainLoop();
    return 0;
}
