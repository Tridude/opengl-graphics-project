attribute vec2 vTexCoord;
varying vec2 texCoord;

attribute vec4 vPosition;
attribute vec3 vNormal;

// output values that will be interpretated per-fragment
varying vec3 fN;
varying vec3 fE;
varying vec3 fL;
varying vec3 r;
varying vec3 position;

uniform vec4 LightPosition;
uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;

//uniform int 

void main()
{
    vec3 pos = (view * model * vPosition).xyz;
    fN = (view*model*vec4(vNormal, 0.0)).xyz;
    fE = -pos;
    fL = (view * LightPosition).xyz - pos;
    //vec3 v = (model*(vPosition - vec4(view[3][0], 0.0, view[3][2], 1.0) )).xyz;
    vec3 v = (model*(vPosition - view[3])).xyz;
    r = reflect(v, normalize(fN)); //2.0*(dot(fN,-v))*fN+v;//
    
    if(LightPosition.w != 0.0) {
        fL = LightPosition.xyz - vPosition.xyz;
    }

    gl_Position = projection * view * model * vPosition;
    texCoord = vTexCoord;
    position = vPosition.xyz;
}
