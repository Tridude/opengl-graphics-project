//
//  camera.h
//  graphics-project
//
//  Created by Contrata, Fred on 11/3/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//

#ifndef __graphics_project__camera__
#define __graphics_project__camera__

#include <cmath>
#include "Angel.h"

// class Camera {
// public:

//     // vec3 m_pos = vec3(0, 0, 3);
//     // vec3 m_dir = vec3(0, 0, 0);
//     // vec3 m_right = vec3(0, 0, 0);
//     // vec3 m_up = vec3(0, 0, 0);

//     // float horz_angle = M_PI;
//     // float vert_angle = 0.0f;
//     // float fov = 45.0f;

//     // float speed = 0.01f;
//     // float mouse_speed = 0.005f;

//     // mat4 get_view_matrix() {
//     //     return LookAt(m_pos, m_pos + m_dir, m_up);
//     // }

//     // void move_mouse(float x_mov, float y_mov) {
//     //     horz_angle += mouse_speed * x_mov;
//     //     vert_angle += mouse_speed * y_mov;

//     //     m_dir = vec3(
//     //         cos(vert_angle) * sin(horz_angle),
//     //         sin(vert_angle),
//     //         cos(vert_angle) * cos(horz_angle)
//     //     );
//     //     m_right = vec3(
//     //         sin(horz_angle - M_PI/2.0f),
//     //         0,
//     //         cos(horz_angle - M_PI/2.0f)
//     //     );
//     //     m_up = cross(m_right, m_dir);

//     // }

//     // void move_forward() {
//     //     m_pos += m_dir * speed;
//     // }

//     // void move_backward() {
//     //     m_pos -= m_dir * speed;
//     // }

//     // void move_right() {
//     //     m_pos += m_right * speed;
//     // }

//     // void move_left() {
//     //     m_pos -= m_right * speed;
//     // }


//     float speed;
//     float angle;

//     mat4 view_matrix;
//     vec3 front;
//     vec3 right;
//     vec3 up;
//     vec3 position;

//     void init_camera() {
//         speed = 0.05f;
//         angle = 0.15f;
//         right = vec3(1, 0, 0);
//         up = vec3(0,  1, 0);
//         front = vec3(0, 0, 1);
//         position = vec3(0, 0, 0);
//     }

//     mat4 get_view_matrix() {
//         view_matrix = mat4;
//         view_matrix[0] = vec4(right, -dot(right, position));
//         view_matrix[1] = vec4(up, -dot(up, position));
//         view_matrix[2] = vec4(front, -dot(front,position));
//         view_matrix[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
//         return view_matrix;
//     }

//     void move() {
//         position += speed * front;
//     }

//     void yaw()

// };

#endif /* defined(__graphics_project__camera__) */
