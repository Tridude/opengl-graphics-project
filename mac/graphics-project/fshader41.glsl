varying  vec2 texCoord;

uniform sampler2D texture_2d;
uniform samplerCube texture_cube;

// per-fragment interpolated values from the vertex shader
varying vec3 fN;
varying vec3 fL;
varying vec3 fE;
varying vec3 r;

varying vec3 position;

uniform vec4 AmbientLight, DiffuseLight, SpecularLight;
uniform vec4 AmbientProduct;
uniform vec4 DiffuseProduct;
uniform vec4 SpecularProduct;
uniform float Shininess;
uniform float alpha;

uniform int texture_type;

void main() 
{ 
    // Normalize the input lighting vectors
    vec3 N = normalize(fN);
    vec3 E = normalize(fE);
    vec3 L = normalize(fL);
    
    vec3 H = normalize( L + E );
    
    // Compute terms in the illumination equation
    vec4 ambient = AmbientLight * AmbientProduct;
    
    vec4 texture_color;

    if (texture_type == 0) {
        texture_color = texture2D(texture_2d, texCoord);
    } else if (texture_type == 1) {
        texture_color = textureCube(texture_cube, r);
    }

    vec4 diffuse = DiffuseLight * DiffuseProduct * texture_color * max(dot(L, N), 0.0);
    //(0.2*pow(length((view * LightPosition).xyz - pos),2.0));

    vec4  specular = SpecularLight * SpecularProduct * pow(max(dot(N, H), 0.0), Shininess);
    
    if (dot(L, N) < 0.0) specular = vec4(0.0, 0.0, 0.0, 1.0);
    
    
    gl_FragColor = ambient + diffuse + specular;
    if (texture_type == 1) {
        gl_FragColor = textureCube(texture_cube, position);
    }
    gl_FragColor.a = alpha;
} 

