attribute vec2 vTexCoord;
varying vec2 texCoord;

attribute vec4 vPosition;
attribute vec3 vNormal;

// output values that will be interpretated per-fragment
varying vec3 fN;
varying vec3 fE;
varying vec3 fL;
varying vec3 r;
varying vec3 position;

uniform vec4 LightPosition;
uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 rotatey180;

uniform int twod;

void main()
{
    vec3 pos = (view * model * vPosition).xyz;
    fN = (view*model*vec4(vNormal, 0.0)).xyz;
    fE = -pos;
    fL = (view * LightPosition).xyz - pos;
    //vec3 v = (model*(vPosition - vec4(view[3][0], 0.0, view[3][2], 1.0) )).xyz;
    vec3 v = (model*(vPosition - view[3])).xyz;
    r = reflect(v, normalize(fN)); //2.0*(dot(fN,-v))*fN+v;//
    
    if(LightPosition.w != 0.0) {
        fL = LightPosition.xyz - vPosition.xyz;
    }

    if (twod == 1) {
        mat4 trans = projection * view * model;
        float d = sqrt(pow(trans[0][0], 2.0) + pow(trans[0][1], 2.0) + pow(trans[0][2], 2.0));
        trans[0][0] = d;
        trans[0][1] = 0.0;
        trans[0][2] = 0.0;
        trans[0][3] = 0.0;
        trans[1][0] = 0.0;
        trans[1][1] = d;
        trans[1][2] = 0.0;
        trans[1][3] = 0.0;
        trans[2][0] = 0.0;
        trans[2][1] = 0.0;
        trans[2][2] = d;
        trans[2][3] = 0.0;
        gl_Position = trans * vPosition;
    } else {
        gl_Position = projection * view * model * vPosition;
    }
    texCoord = vTexCoord;
    position = vPosition.xyz;
}
