//
//  collision_box.h
//  graphics-project
//
//  Created by Contrata, Fred on 11/7/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//

#ifndef __graphics_project__collision_box__
#define __graphics_project__collision_box__

class CollisionBox {
public:
    float pos_x;
    float pos_y;
    float pos_z;
    float width;
    float depth;
    float height;

    bool collides(float other_x, float other_y, float other_z, float other_width, float other_depth, float other_height);

    //bool collides (CollisionBox other);
};


#endif /* defined(__graphics_project__collision_box__) */
