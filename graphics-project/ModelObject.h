//
//  ModelObject.h
//  graphics-project
//
//  Created by Contrata, Fred on 11/14/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//

#ifndef __graphics_project__ModelObject__
#define __graphics_project__ModelObject__

#include "Angel.H"
#include "CubeObject.h"
#include "SphereObject.h"
#include "CollisionBox.h"


typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

class ModelObject {
public:
	CubeObject cube_objects[10];
	int cube_count=0;
	SphereObject sphere_objects[10];
	int sphere_count=0;
    CollisionBox collision_boxes[10];
    int collision_count=0;
};


#endif /* defined(__graphics_project__ModelObject__) */
