//
//  CubeObject.h
//  graphics-project
//
//  Created by Contrata, Fred on 11/12/14.
//  Copyright (c) 2014 Fred. All rights reserved.
//

#ifndef __graphics_project__CubeObject__
#define __graphics_project__CubeObject__

#include "Angel.h"

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

class CubeObject {
public:
    float pos_x = 0.0;
    float pos_y = 0.0;
    float pos_z = 0.0;
    float rot_x = 0.0;
    float rot_y = 0.0;
    float rot_z = 0.0;
    float sca_x = 1.0;
    float sca_y = 1.0;
    float sca_z = 1.0;
    float alpha = 1.0;

    color4 material_ambient;
    color4 material_diffuse;
    color4 material_specular;
    int texture_index=0;
    int animation_index=0;
	int twod = 0;
    float material_shininess;

    // CubeObject(){}

    mat4 GetModelMatrix();

};

#endif /* defined(__graphics_project__CubeObject__) */
